# Banjo Synth

A little web-based banjo player. Just pick up your keyboard, and pretend it's
a banjo! (kind of...)

[Try it online!](https://zach-geek.gitlab.io/banjo-synth)
