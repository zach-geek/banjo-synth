require(["ADC", "JSON"])

isRecording <- false;
function startRecording()
{
  require("WebSocket");
  server <- WebSocketServer();

  print("waiting for connection\n");
  while (!server.available())
    delay(10);

  recordingSocket <- server.accept();
  print("connected\n");
  recordingSocket.binary(false);
  isRecording = true
}

function report(string, fret)
{
  print("String: " + string + " Fret: " + fret + "\n")

  if (isRecording && recordingSocket.isconnected())
  {
    recordingSocket.writestr(jsonencode({string = string, fret = fret}))
  }
}

class BanjoString {
  adc = null
  channel = 0
  lastFret = 0
  stringNum = 0
  voltages = null
  constructor (stringNumber, adcNum, channelNum) {
    adc = ADC(adcNum)
    stringNum = stringNumber
    channel = channelNum

    if (channelNum == 0) {
      voltages = [0, 2.3, 3.0, 3.3]
    } else {
      voltages = [0, 2.8, 3.15, 3.3]
    }
  }

  function voltToFret(v) {

    const tolerance = 0.06

    local fret = 0;

    foreach (ref in voltages) {
      if (v > ref - tolerance && v < ref + tolerance) {
        break;
      }

      fret++;
    }

    if (fret >= voltages.len())
    {
      fret = -1;
    }

    return fret;
  }

  function check() {
    local v = adc.readv(channel)
    local newFret = voltToFret(v)

    if (newFret != lastFret && newFret >= 0)
    {
      print("V: " + v + "\n")
      report(stringNum, newFret)
      lastFret = newFret
    }
  }
}

strings <- [
  BanjoString(1, 0, 0),
  BanjoString(2, 0, 1),
  BanjoString(3, 1, 0),
  BanjoString(4, 1, 1)
]

while (true) {
  foreach (s in strings) {
    s.check()
  }

  delay(10)
}
